/*
Some utility commands:
- `nix flake update --commit-lock-file`
- `nix flake lock update-input <input>`
- `nix build .#septadrop` or `nix build .`
- `nix run .#septadrop` or `nix run .`
*/

{
  description = "A block game made in Rust and SFML.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, rust-overlay }:
    let
      overlays = [ (import rust-overlay) ];
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system overlays;
      };
      rustSettings = with pkgs; {
        src = ./.;
        cargoHash = nixpkgs.lib.fakeHash;
      };
      meta = with nixpkgs.lib; {
        homepage = "https://git.elnu.com/ElnuDev/septadrop";
        license = [ licenses.gpl3 ];
        platforms = [ system ];
        maintainers = with maintainers; [ elnudev ];
      };
    in {
      devShells.${system}.default = with pkgs; mkShell {
        packages = [
          (pkgs.rust-bin.stable.latest.default.override {
            extensions = [ "rust-src" ];
          })
          cargo-edit
          bacon
        ];
        inputsFrom = with self.packages.${system}; [ septadrop ];
      };
      packages.${system} = {
        default = self.packages.${system}.septadrop;
        septadrop = let
          version = "1.1.0";
        in pkgs.rustPlatform.buildRustPackage (rustSettings // {
          pname = "septadrop";
          inherit version;
          buildAndTestSubdir = "septadrop";
          buildInputs = with pkgs; [ csfml ];
          cargoHash = "sha256-9GoBHchjmgyIpvLlmNPdCaC378jA4zZqKQYj2hajNlU=";
          postPatch = ''
            pushd septadrop
            sed -i -E "s|\"res\"|\"''${out}\/share\"|" src/config.rs
            mkdir -p $out/share
            cp -r build/septadrop_${version}_amd64/usr/share/* $out/share
            cp -r res/* $out/share
            popd
          '';
          meta = meta // {
            description = "A block game made in Rust and SFML.";
          };
        });
      };
    };
}
